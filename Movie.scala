import java.time.Year

import scala.annotation.tailrec

case class Movie(id: Int, name: String, yearReleased: Year)

/** API to fetch Movies */
trait Api {

  /** Given a page number, fetches movies on that page (empty if page is past last page) */
  def fetchBatch(page: Int): Iterator[Movie]
}

object Movie {

  /** @param api API that fetches movies by page
    * @return A map with keys representing year released and values as movie names
    *
    * Iterate over pages by recursively calling fetch and incrementing the page with
    * each recursive call. Merge the results of namesByYear(movies) into the final
    * results at each iteration. Return the final result when there are no more pages.
    * (The results of api.fetchBatch(page) is empty)
    */
  def namesByYear(api: Api): Map[Year, Set[String]] = {
    @tailrec
    def fetch(page: Int, result: Map[Year, Set[String]]): Map[Year, Set[String]] = {
      val movies = api.fetchBatch(page)
      if(movies.hasNext) {
        fetch(page + 1, result ++ namesByYear(movies))
      } else {
        result
      }
    }
    fetch(1, Map.empty[Year, Set[String]])
  }

  /** @param movies A list of movies to be indexed by year released
    * @return A map with keys representing year released and values as movie names
    *
    * Overloaded method that folds over an iterator of movies to create a map indexing
    * the movie names by year released.
    */
  private def namesByYear(movies: Iterator[Movie]): Map[Year, Set[String]] = {
    movies.foldLeft(Map.empty[Year, Set[String]]){ (resultSoFar, movie) =>
      resultSoFar.get(movie.yearReleased) match {
        case Some(moviesFound) => resultSoFar + (movie.yearReleased -> (moviesFound + movie.name))
        case None => resultSoFar + (movie.yearReleased -> Set(movie.name))
      }
    }
  }
}

