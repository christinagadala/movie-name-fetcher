# Movie Name Fetcher

You are given the following Scala API that can fetch movies page by page from an online movie catalogue:

```scala
import java.time.Year

case class Movie(id: Int, name: String, yearReleased: Year)

/** API to fetch Movies */
trait Api {
  /** Given a page number, fetches movies on that page (empty if page is past last page) */
  def fetchBatch(page: Int): Iterator[Movie]
}
```

Your task is to write a function to fetch all distinct movie names by year:

```scala
def namesByYear(api: Api): Map[Year, Set[String]] = ???
```
Here is one attempt:

```scala
object Movie {
  def namesByYear(api: Api): Map[Year, Set[String]] = {
    var data = Map[Year, Set[String]]()
    var i = 1
    var isLastPage = false

    while(!isLastPage) {
      var response = api.fetchBatch(i).toList

      if (response.length == 0) {
        isLastPage = true
      } else {
        for {movie <- response} {
          try {
            data(movie.yearReleased) = data(movie.yearReleased) + movie.name
          } catch {
            case _ => data(movie.yearReleased) = Set(movie.name)
          }
        }
        i = i + 1
      }
    }

    return data
  }
}

```

Can you rewrite the above code to be more efficient idiomatic Scala?
